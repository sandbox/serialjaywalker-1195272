<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Defines actions to be used in to Commerce POS. An action defines a way to
 * process data in the POS form and perform subsequent page builds.
 *
 *  @return
 *  An array of actions, each defined by an array with the following keys:
 * - 'action text' - a short string to be used to allow the action to be used
 *   with a programmable keyboard or barcode.
 * - 'callbacks' - An array of callback functions with the following optional keys:
 *    - 'access' - Each function should takes one parameter, $data. If any access callback
 *      returns FALSE, the action cannot be completed.
 *    - 'parse' - Each fun takes two parameters, $input and $action_text, and determines whether this action
 *      should process the form. This is only called if no action has been set by other means. The
 *      action will handle this form submission if any of the parse callbacks returns true.
 *    - 'extract' - Each callback takes two parameters, &$data and $values. Once it has been determined that this action will handle the form submission,
 *      this callback extracts the needed information from the array $values of submitted form values
 *      and stores them to the array $data.
 *    - 'validate' - Takes a parameter, &$data, and returns any errors (other than access errors).
 *    - 'execute' - Takes the parameter &$data and completes the form submission.
 *    - 'build' - Takes the parameter &$data and returns an array of form (or render) elements
 *      to be sent to the browser.
 * - 'weight' - A number used to order the actions.
 * - 'button' - An array used to generate a button for the POS screen. The following keys may be used.
 *    - 'label' - The visible text on the button.
 *    - 'section' - a string used for organizing the buttons
 *    - 'weight' - a weight used for sorting buttons.
 * @see hook_commerce_pos_action_info_alter()
 */
function hook_commerce_pos_action_info() {
    return array(
    'commerce_pos_clear' => array(
      'action text' => 'CL',
      'callbacks' => array(
        'parse' => array('commerce_pos_clear_parse'),
        'execute' => array('commerce_pos_clear_execute'),
      ),
      'button' => array(
        'label' => t('Clear'),
        'section' => 'order',
       ),
      'weight' => 50,
    ),
  );
}

/**
 * Alter pos actions.
 *
 * Modules have an opportunity to change action information, for example, to change callbacks.
 *
 * @see hook_commerce_pos_action_info()
 */
function hook_commerce_pos_action_info_alter(&$info) {
  //No example yet.
}

/**
 * Defines buttons for Commerce POS.
 *
 * @return
 * an array of sections of buttons. A section, is, itself, just an array of buttons.
 * Each button is represented by an array of render elements of
 * #type commerce_pos_button. In addition to the usual render array keys, a commerce_pos_button
 * element might include some of the following:
 *  -'#action' - the identifier for the commerce_pos_action that should be set by this button
 *  -'#append_text' - a text string to be appended to the content of the input box when the
 *    button is pressed.
 *  -'#replace_text' - a text string to replace the current content of the input box when the button
 *    is pressed.
 *
 * @see hook_commerce_pos_button_info_alter()
 */
function hook_commerce_pos_button_info() {
  //Here we'll just generate a number pad with * and .
  $buttons = array();
  for ($i = 0; $i <=9; ++$i) {
    $buttons['num_pad'][$i] = array(
      '#title' => $i,
      '#append_text' => $i,
      '#weight' => $i,
      '#type' => 'commerce_pos_button',
    );
  }
  //Following phone/calculator/keyboard convention, make the 0 button last.
  $buttons['num_pad'][0]['#weight'] = 11;
  //Now the * and .
  $buttons['num_pad']['qty'] = array(
    '#title' => '*',
    '#append_text' => '*',
    '#weight' => 10,
    '#type' => 'commerce_pos_button',
  );
  $buttons['num_pad']['dec'] = array(
    '#title' => '.',
    '#append_text' => '.',
    '#weight' => 12,
    '#type' => 'commerce_pos_button',
  );

  $actions = commerce_pos_get_action_info();
  //Return buttons from actions with the appropriate keys set.
  foreach ($actions as $action_name => $action) {
    if (isset($action['button'])) {
      $button = array(
        '#type' => 'commerce_pos_button',
        '#action' => $action['name'],
        '#submit_form' => TRUE,
        '#title' => $action['button']['title'],
        '#weight' => empty($action['button']['weight']) ? NULL : $action['button']['weight'],
      );
      $buttons[$action['button']['section']][] = $button;
    }
  }
  return $buttons;
}

/**
 * Alter pos buttons.
 *
 * @see hook_commerce_pos_button_info()
 */
function hook_commerce_pos_button_info_alter(&$info) {
  //No example yet.
}

/**
 * Extract variables from the POS form.
 *
 * This hook is called before the current action's extract callbacks. Together with
 * hook_commerce_pos_build(), it allows modules to have variables persist between page views,
 * even when their own actions do not have control of the form.
 *
 * @param $data
 *  An array of POS variables.
 * @param $values
 *  An array of values submitted by the POS form.
 *
 * @see hook_commerce_pos_build()
 * @see hook_commerce_pos_action_info()
 */
function hook_commerce_pos_extract(&$data, $values) {
  if(!empty($values['order']['order_id'])) {
    $data['order'] = commerce_order_load($values['order']['order_id']);
  }
}

/**
 * Add elements to the POS form.
 *
 * This hook is called before the current action's build callbacks. Together with
 * hook_commerce_pos_extract(), it allows modules to have variables persist between page views,
 * even when their own actions do not have control of the form.
 *
 * @param $data
 *  An array of POS variables.
 *
 * @return
 *  An array of form elements to be sent to the browser.
 *
 * @see hook_commerce_pos_build()
 * @see hook_commerce_pos_action_info()
 */
function hook_commerce_pos_build($data = array()) {
  $return_val = array();

  if (!empty($data['order'])) {
    $order = $data['order'];

    $return_val['order'] = array(
      '#type' => 'commerce_pos_order',
      '#title' => t('Order'),
      '#order' => $order,
      '#default_value' => $data['order_new_value'],
    );
  }
  return $return_val;
}