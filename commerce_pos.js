
(function($) {

  /**
   * Click the submit button when enter is pressed and bind actions to button
   */

  Drupal.behaviors.commercePos = {
      attach: function(context, settings) {
        //Silly workaround for #1059100: Store list of CSS files before it is cleared and
        //leave it somewhere that we can find it later.
        $.each(Drupal.settings.ajaxPageState.css, function(key, value) {
          Drupal.settings.allStyles = Drupal.settings.ajaxPageState.css;
          return false;
        });
        //Receipt printing
        var errors = $('.error', context);

        if (errors.length == 0) {
          var receipt = $('.commerce-pos-receipt:not(.commerce-pos-receipt-unstyled)', context);
          var unstyledReceipt = $('.commerce-pos-receipt-unstyled', context);
          
          if(unstyledReceipt.length > 0) {
            unstyledReceipt.once('print-receipt', function() {
              $(this).printElement({overrideElementCSS:true});
              $.getJSON(Drupal.settings.basePath + 'admin/commerce/pos/drawer_open');
            });
          }
          
          if(receipt.length > 0) {
            receipt.once('print-receipt', function() {
              $(this).printElement();
              $.getJSON(Drupal.settings.basePath + 'admin/commerce/pos/drawer_open');
            });
          }
        }
        
        var input = $('.commerce-pos-input');
        if (!window.escKeyProcessed) {
          window.escKeyProcessed = true;
          $(window).keydown(function(event) {
            if (event.keyCode == 27) {
              input = $('.commerce-pos-input');
              if (input.val() != '' && !isNaN(input.val())) {
                input.val(input.val() + '*');
              }
              $('.commerce-pos-input').focus();              
            }
          });
        }
        $('.commerce-pos-panel-set input[type="text"]').once('select-on-click').click(function() {
          $(this).select();
        });
        $('.commerce-pos-button', context).once('button-action').mousedown(function() {
          //TODO: There's probably a slick way to avoid querying again by using closures
          var text = $('.commerce-pos-input');
          var replaceText = $('.commerce-pos-button-replace-text', $(this)).html();
          var appendText = $('.commerce-pos-button-append-text', $(this)).html();
          var action = $('.commerce-pos-button-action', $(this)).html();
          if(action && action.length > 0) {
            $('.commerce-pos-new-action-name').val(action);
          }
          if(replaceText && replaceText.length > 0) {
            text.val(replaceText).focus();
          }
          if(appendText && appendText.length > 0) {
            text.val(text.val() + appendText).focus();
            //TODO: do away with the following, which can cause problems
            //but exists to prevent subsequent events from stealing focus away.
            return false;
          }

        }).button();

        $('.commerce-pos-button-submit', context).once('submit').mousedown(function() {
          $('.commerce-pos-ajax-trigger').click();
        }).button();

        $('.commerce-pos-input', context).select();

        var dialog = $('.commerce-pos-modal-visible', context);
        var parentForm = dialog.parents('form');
        var messages = $('#commerce-pos-replace-wrapper .messages', context);
        if (dialog.length && messages.length) {
          dialog.once('move-messages').prepend(messages);
        }
        dialog.once('commerce-pos-modal')
        .each(function() {
          var currentDialog = $(this);
          currentDialog.dialog({modal: true, closeOnEscape: false, position:'top'});
        });
        dialog.parent().appendTo(parentForm);
        $("input[type='submit']", dialog).once('modal').click(function() {dialog.dialog('close'); dialog.removeClass('commerce-pos-modal-visible');event.preventDefault();$('.commerce-pos-ajax-trigger').click();});
        $('.commerce-pos-modal-visible .focus', context).focus();
        $('.commerce-pos-new-action-name', context).val('');
        $('.commerce-pos-panel-set').once('commerce-pos-panels-tabs').tabs({
            show: function(event, ui){$('input[type=text]', ui.panel).focus();},
            selected:0
        });
        $('.commerce-pos-input-clear').mousedown(function() {
          input.val('').focus();
        }).button({text:false, icons:{primary:'ui-icon-closethick'}});
        //Enter button.
        $('.commerce-pos-input-enter').once('button-enter').mousedown(function() {
          $('.commerce-pos-ajax-trigger').click();
        }).button();
      }
  };
})(jQuery);
