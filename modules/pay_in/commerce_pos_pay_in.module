<?php

/**
 * @file
 * Defines a payment method that should not be associated with any order, to track
 * cash that may be put into the cash drawer for some reason other than as payment
 * for an order.
 */

/**
 * Implements hook_commerce_pos_payment_method_info().
 */
function commerce_pos_pay_in_commerce_pos_payment_method_info() {
  return array(
    'commerce_pos_pay_in' => array(
      'method_id' => 'commerce_pos_pay_in',
      'title' => t('Pay in'),
      'short_title' => t('Pay in'),
      'description' => t('In or out flow of cash not associated with any order.'),
      'pos' => array(
        'button text' => t('Pay in'),
        'action text' => 'PI',
        'cash' => TRUE,
        'button section' => 'misc',
        'callbacks' => array(
          'build' => array('commerce_pos_pay_in_build'),
          'access' => array('commerce_pos_pay_in_access'),
          'parse' => array('commerce_pos_payment_parse'),
          'extract' => array('commerce_pos_pay_in_extract'),
          'execute' => array('commerce_pos_pay_in_execute'),
          'validate' => array('commerce_pos_pay_in_validate'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_permissions().
 */
function commerce_pos_pay_in_permission() {
  return array(
    'add pay in' => array(
      'title' => t('Add POS pay in/out'),
    ),
  );
}

/**
 * Access callback for pay in command
 */
function commerce_pos_pay_in_access(&$data) {
  return user_access('use commerce pos') && user_access('add pay in');
}

/**
 * Extract callback for the pay in command.
 */
function commerce_pos_pay_in_extract(&$data, $values) {
  //First the case in which the user already has the dialog.
  if ($values['op'] == t('Cancel')) {
    $data['cancel'] = TRUE;
    return;
  }
  if (isset($values['dialog_submission'])) {
    $decimal = $values['pay_in_decimal'];
    $data['pay_in_message'] = $values['pay_in_message'];
    $data['dialog_submission'] = TRUE;
  }
  //Now the case in which we'll need more information.
  else if (empty($values['new_action_name'])) {
    $action_text = $data['action']['action text'];
    $decimal = drupal_substr($data['input'], 0, drupal_strlen($data['input']) - drupal_strlen($action_text));
  }
  //Now the case in which the action name has been submitted in the hidden element.
  else {
    $decimal = $data['input'];
  }
  //TODO: add the option of setting the currency.
  $data['charge']= array(
      'decimal' => $decimal,
      'currency_code' => commerce_pos_default_currency()
  );
}

/**
 * Validate callback for the pay in command.
 */
function commerce_pos_pay_in_validate(&$data) {
  if (empty($data['cancel'])) {
    //Rather than return errors, we'll just prompt for the correct information.
    $dialog_required = FALSE;
    if (!is_numeric($data['charge']['decimal'])) {
      $dialog_required = TRUE;
    }
    else {
      $data['charge']['amount'] = commerce_currency_decimal_to_amount($data['charge']['decimal'], $data['charge']['currency_code']);
    }
    if (empty($data['pay_in_message'])) {
      $dialog_required = TRUE;
    }
    $data['dialog_required'] = $dialog_required;
  }
}

/**
 * Execute callback for the pay in command.
 */
function commerce_pos_pay_in_execute(&$data) {
  if (empty($data['cancel']) && empty($data['dialog_required'])) {
    $transaction = commerce_pos_pay_in_transaction($data['charge'], $data['message']);
    commerce_payment_transaction_save($transaction);
    $data['completed'] = TRUE;
    drupal_set_message(t('The pay in has been recorded'));
  }
}

/**
 * Build callback for the pay in command.
 */
function commerce_pos_pay_in_build(&$data) {

  if(empty($data['completed']) && empty($data['cancel'])) {
    $elements['action_name'] = array(
      '#type' => 'value',
      '#value' => 'commerce_pos_pay_in',
    );
    $elements['dialog_submission'] = array(
      '#type' => 'value',
      '#value' => TRUE,
    );
    $elements['pay_in_dialog'] = array(
      '#type' => 'commerce_pos_modal',
      '#cancel' => TRUE,
      '#submit_text' => t('Process'),
      '#visible' => TRUE,
    );

    $elements['pay_in_dialog']['pay_in_decimal'] = array(
      '#type' => 'textfield',
      '#title' => t('Decimal amount'),
      '#default_value' => $data['charge']['decimal'],
      '#attributes' => empty($data['charge']['decimal']) ? array('class' => array('focus')) : array(),
      '#size' => 5,
    );

    $elements['pay_in_dialog']['pay_in_message'] = array(
      '#type' => 'textfield',
      '#title' => t('Reason'),
      '#size' => 20,
      '#attributes' => !empty($data['charge']['decimal']) ? array('class' => array('focus')) : array(),
      '#description' => t('Provide a reason for this pay in.'),
    );

    return $elements;
  }
  if($data['completed']) {
    return array('empty_receipt'=> array(
      '#type' => 'commerce_pos_receipt',
      'content' => array('#markup' => t('No content')),
    ));
  }
}

/**
 * Create a new POS pay in transaction in the specified amount/currency.
 * @param $method_id
 *  The method_id of the payment method to use
 * @param $charge
 *  An array with keys 'amount' and 'currency_code'
 *
 * @return
 *  The new transaction, not yet saved in the database
 */
function commerce_pos_pay_in_transaction($charge, $message) {
  $transaction = commerce_payment_transaction_new('commerce_pos_pay_in');
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->status = 'commerce_pos_pay_in_complete';
  $transaction->message_variables = array($message);
  return $transaction;
}

/**
 * Implements hook_commerce_payment_transaction_status_info().
 */
function commerce_pos_pay_in_commerce_payment_transaction_status_info() {
  return array('commerce_pos_pay_in_complete' => array(
    'status' => 'commerce_pos_pay_in_complete',
    'title' => t('Complete'),
    'icon' => drupal_get_path('module', 'commerce_payment') . '/theme/icon-success.png',
    'total' => TRUE,
    ),
  );
}