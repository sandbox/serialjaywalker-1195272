
(function($) {

  Drupal.behaviors.commercePosOpenRing = {
      attach:function(context, settings) {
        //TODO: Create a button at the bottom and make it load the open rings.
        var parent = $('.commerce-pos-search-results-container-product', context).parent();
        $('<button class = "commerce-pos-open-ring-search">' + Drupal.t("Can\'t find it?") + '</button>').appendTo(parent);
        $('.commerce-pos-open-ring-search', parent).once('open-ring').button()
        .click(function() {
          var panel = $(this).parents('.commerce-pos-search-panel');
          var searchName = $('.commerce-pos-search-form-search-name', panel).val();
          var query = $('.commerce-pos-search-form-query', panel).val();
          var path = Drupal.settings.basePath + '?q=commerce_pos_search/product/&f[0]=type:pos_open_ring';
          $.getJSON(path, function(response) {
            var container = $('.commerce-pos-search-results', panel);
            //Update the search results.
            Drupal.detachBehaviors(container);
            container.html(response.results);
            Drupal.attachBehaviors(container);
            //Update the facets.
            var facets = $('.commerce-pos-search-facets', panel);
            Drupal.detachBehaviors(facets);
            facets.html(response.facets);
            Drupal.attachBehaviors(facets);
            //Store the facet names.
            if (typeof Drupal.settings.commercePosSearch.facets != "array") {
              Drupal.settings.commercePosSearch.facets = {};
            }
            Drupal.settings.commercePosSearch.facets[searchName] = response.facetNames;            
          });
          
          $('.commerce-pos-search-form-query', panel).val('');
          $('.facetapi-commerce-pos-search-radios:checked', panel).attr('checked', false);
          $('input.facetapi-facet-type[value=pos_open_ring]', panel).attr('checked', true);
          Drupal.commercePosSearch.loadResults(searchName, false);
          });
      }
  };

})(jQuery);