<?php

/**
 * @file
 * Rules integration for adding and removing customers at the POS
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_event_info().
 */
function commerce_pos_customer_rules_event_info() {
  $events = array();

  $events['commerce_pos_customer_set_customer'] = array(
    'label' => t('Setting the customer in the POS.'),
    'group' => t('Commerce POS Customer'),
    'variables' =>  entity_rules_events_variables('commerce_order', t('Commerce order'), TRUE),
    'access callback' => 'commerce_order_rules_access',
  );

  return $events;
}

 /*
  * @}
  */
