(function ($) {

  Drupal.behaviors.commercePosCustomer = {
      attach: function (context, settings) {
        //Customer buttons.
        var customerAddFields = $('.views-field-pos-add-customer', context);
        customerAddFields.once('customer-add-button').each(function() {
          var trigger = $('.commerce-pos-ajax-trigger');
          customer = $(this).parent();
          var customerNumber = $('.commerce-pos-customer-number', this).html();
          customer.once('customer-button').mousedown(function() {
            $('.commerce-pos-input').val(customerNumber);
            $('.commerce-pos-new-action-name').val('commerce_pos_customer');
            trigger.click();
          }).button();        
        });
        //Copy the value from the custom form to the views form.
        $('.commerce-pos-customer-select-customer-name', context).once('copy').keyup(function (e) {
          $('#edit-commerce-pos-customer-name-value').val($(this).val()).trigger(e);
        }).keydown(function(event) {
          //Don't submit on pressing enter.
          if (event.which == 13) {
            event.preventDefault();
          }
        });
      }
  };
})(jQuery);
