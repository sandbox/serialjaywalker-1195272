<?php

/**
 * @file
 * Defines a token for the discount rate name corresponding to
 * a particular taxonomy term.
 */

/**
 * Implements hook_token_info().
 */
function commerce_pos_customer_taxonomy_discount_token_info_alter(&$data) {

	$data['tokens']['term']['discount-component-name'] = array(
  	'name' => t('Discount price component.'),
    'description' => t('The machine name of the price component corresponding to the discount for this taxonomy term.'),
  );
}

/**
 * Implements hook_tokens().
 */
function commerce_pos_customer_taxonomy_discount_tokens($type, $tokens, $data = array(), $options = array()) {
  $url_options = array('absolute' => TRUE);

  if (isset($options['language'])) {
    $url_options['language'] = $options['language'];
    $language_code = $options['language']->language;
  }
  else {
    $language_code = NULL;
  }

  $replacements = array();

  if ($type == 'term' && !empty($data['term'])) {
    $term = $data['term'];

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'discount-component-name':
          $replacements[$original] = 'commerce_pos_customer_taxonomy_discount|' . $term->tid;
          break;
      }
    }
  }

  return $replacements;
}