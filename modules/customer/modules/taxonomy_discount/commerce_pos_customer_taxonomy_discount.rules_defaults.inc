<?php

/**
 * @file
 * Default rules configuration for applying discounts for classes of customers.
 */

function commerce_pos_customer_taxonomy_discount_default_rules_configuration() {

  $rule = rules_reaction_rule();
  $rule->label = t('Recalculate line item price upon changing user.');

  $loop_settings = array(
    'list:select' => 'commerce-order:commerce-line-items',
    'item:var' => 'line_item',
    'item:label' => t('Current line item'),
  );

  $loop = rules_loop($loop_settings)
    ->action('component_commerce_pos_line_item_calculate_price', array(
    	'commerce_line_item:select' => 'line-item',
      'commerce_order:select' => 'commerce_order',
    ))
    ->action('entity_save', array(
      'data:select' => 'line-item',
    ));

  $rule->event('commerce_pos_customer_set_customer')
    ->action($loop);

  return array('commerce_pos_customer_recalculate_prices' => $rule);
}

function commerce_pos_customer_taxonomy_discount_default_rules_configuration_alter($rules) {
  if (!empty($rules['commerce_pos_line_item_calculate_price'])) {
    $rule = rule();
    $rule->label = t('Apply customer taxonomy discounts.');
    $rule->weight = -8;

    $rule->condition('data_is', array(
      'data:select' => 'commerce_line_item:type',
      'value' => 'product',
        ))
      ->condition('entity_has_field', array(
        'entity:select' => 'commerce-order',
        'field' => 'commerce_customer_pos',
        ))
      ->condition(rules_condition('data_is_empty', array(
        'data:select' => 'commerce-order:commerce-customer-pos',
        ))->negate())
      ->condition('entity_has_field', array(
        'entity:select' => 'commerce-order:commerce-customer-pos',
        'field' => 'commerce_pos_customer_category',
        ))
      ->condition(rules_condition('data_is_empty', array(
        'data:select' => 'commerce-order:commerce-customer-pos:commerce-pos-customer-category',
        ))->negate())
      ->condition('entity_has_field', array(
        'entity:select' => 'commerce-order:commerce-customer-pos:commerce-pos-customer-category',
        'field' => 'commerce_pos_customer_discount',
        ))
      ->condition(rules_condition('data_is_empty', array(
        'data:select' => 'commerce-order:commerce-customer-pos:commerce-pos-customer-category:commerce-pos-customer-discount',
        ))->negate())
      ->action('commerce_line_item_unit_price_multiply', array(
          'commerce_line_item:select' => 'commerce-line-item',
          'amount:select' => 'commerce-order:commerce-customer-pos:commerce-pos-customer-category:commerce-pos-customer-discount',
          'component_name:select' => 'commerce-order:commerce-customer-pos:commerce-pos-customer-category:commerce-pos-customer-taxonomy-discount-price-component',
          'round_mode' => COMMERCE_ROUND_NONE,
      ));


    $rules['commerce_pos_line_item_calculate_price']->rule($rule);
  }
}