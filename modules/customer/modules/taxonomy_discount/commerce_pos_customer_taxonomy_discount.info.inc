<?php

/**
 * @file
 * Adds the price component name to a taxonomy term.
 */

/**
 * Implements hook_entity_property_info().
 */
function commerce_pos_customer_taxonomy_discount_entity_property_info_alter(&$info) {
  $info['taxonomy_term']['properties']['commerce_pos_customer_taxonomy_discount_price_component'] = array(
    'type' => 'text',
    'label' => t('Price component name'),
    'description' => t('The machine readable name of the corresponding price component.'),
    'getter callback' => 'commerce_pos_customer_taxonomy_discount_component_name',
  );
}