<?php

/**
 * @file
 * Rules integration for orders in commerce POS
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_pos_line_item_discount_rules_action_info() {
  $actions = array();

  $actions['commerce_pos_line_item_discount_apply'] = array(
    'label' => t('Apply line item discounts'),
    'group' => t('Commerce POS Line Item'),
    'parameter' => array(
      'commerce_line_item' => array('type' => 'commerce_line_item', 'label' => t('Line item', array(), array('context' => 'a drupal commerce line item'))),
    ),
    'callbacks' => array( 'execute' => 'commerce_pos_line_item_discount_rules_apply_discounts'),
  );

  return $actions;
}

/**
 * Implements hook_rules_event_info().
 */
function commerce_pos_line_item_discount_rules_event_info() {
  $events = array();

  $events['commerce_pos_line_item_discount_add'] = array(
    'label' => t('Add a line item discount.'),
    'group' => t('Commerce POS Line Item'),
    'variables' =>  array_merge(entity_rules_events_variables('commerce_line_item', t('Commerce line item'), TRUE),
      entity_rules_events_variables('commerce_order', t('Commerce order'), TRUE)),
    'access callback' => 'commerce_order_rules_access',
  );

  return $events;
}


/**
 * Execute callback for printing a receipt.
 */
function commerce_pos_line_item_discount_rules_apply_discounts($line_item, $action_settings, $rule_state, $action, $callback_type) {
  commerce_pos_line_item_discount_apply_discounts($line_item);
}



 /*
  * @}
  */
