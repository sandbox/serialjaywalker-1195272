<?php

/**
 * @file
 * Default rules configuration for adding and removing line items.
 */

function commerce_pos_line_item_discount_default_rules_configuration() {
  $rule = rules_reaction_rule();
  $rule->label = t('Recalculate price on adding line item discount.');
  $rule->active = TRUE;

  $rule->event('commerce_pos_line_item_discount_add')
    ->action('component_commerce_pos_line_item_calculate_price', array(
    	'commerce_line_item:select' => 'commerce-line-item',
      'commerce_order:select' => 'commerce-order',
    ));
  $rule->weight = -9;

  $rules['commerce_pos_line_item_discount_recalculate'] = $rule;

  return $rules;
}

function commerce_pos_line_item_discount_default_rules_configuration_alter(&$rules) {
  if (!empty($rules['commerce_pos_line_item_calculate_price'])) {
    $rule = rule();
    $rule->weight = -9;
    $rule->label = t('Apply POS line item discounts');

    $rule->condition('entity_has_field', array(
        'entity:select' => 'commerce-line-item',
        'field' => 'commerce_pos_line_item_discounts',
        ))
      ->action('commerce_pos_line_item_discount_apply', array(
          'commerce_line_item:select' => 'commerce-line-item',
      ));

    $rules['commerce_pos_line_item_calculate_price']->rule($rule);
  }
}