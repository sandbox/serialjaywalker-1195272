<?php

/**
 * @file
 * Default rules configuration for commerce_pos_payment module
 */

function commerce_pos_payment_default_rules_configuration() {

  $variables = array(
  	'commerce_order' => array(
  		'type' => 'commerce_order',
  		'label' => t('Order')
    ),
    'commerce_payment_transaction' => array(
      'type' => 'commerce_payment_transaction',
      'label' => t('Payment transaction'),
    ),
    'errors' => array(
      'type' => 'list<text>',
      'label' => t('Errors'),
      'parameter' => FALSE,
    ),
  );
  $provides = array('errors');
  $rule_set = rules_rule_set($variables, $provides);
  $rule_set->label = t('Attempt to add a payment in the POS');

  $rule = rule();
  $rule->label = t('Prevent positive balance/negative payment');
  $rule->active = TRUE;
  $rule->condition('commerce_payment_order_balance_comparison', array(
        'commerce_order:select' => 'commerce-order',
        'operator' => '>',
        'value' => '0',
      ))
    ->condition('data_is', array(
        'data:select' => 'commerce-payment-transaction:amount',
        'op' => '<',
        'value' => '0',
      ))
    ->action('list_add', array(
        'list:select' => 'errors',
        'item' => t('You cannot add a refund when a balance is due.'),
      ));
  $rule_set->rule($rule);

  $rule = rule();
  $rule->label = t('Prevent negative balance/positive payment');
  $rule->condition('commerce_payment_order_balance_comparison', array(
        'commerce_order:select' => 'commerce-order',
        'operator' => '<',
        'value' => '0',
      ))
    ->condition('data_is', array(
        'data:select' => 'commerce-payment-transaction:amount',
        'op' => '>',
        'value' => '0',
      ))
    ->action('list_add', array(
        'list:select' => 'errors',
        'item' => t('You cannot add a payment when the balance is negative.'),
      ));
  $rule_set->rule($rule);

  $rule = rule();
  $rule->label = t('Prevent zero balance/nonzero payment');
  $rule->active = TRUE;
  $rule->condition('commerce_payment_order_balance_comparison', array(
        'commerce_order:select' => 'commerce-order',
        'operator' => '=',
        'value' => '0',
      ))
    ->condition(rules_condition('data_is', array(
        'data:select' => 'commerce-payment-transaction:amount',
        'op' => '==',
        'value' => '0',
        ))->negate())
    ->action('list_add', array(
        'list:select' => 'errors',
        'item' => t('You cannot add a payment when the balance is zero.'),
      ));
  $rule_set->rule($rule);

  //Check the order status before adding a payment.
  $rule = rule();
  $rule->label = t('Disallow adding payment transaction when status is not in progress');
  $rule->active = TRUE;
  $rule->condition(rules_condition('data_is', array(
        'data:select' => 'commerce-order:status',
        'op' => '==',
        'value' => 'pos_in_progress',
        ))->negate())
    ->action('list_add', array(
        'list:select' => 'errors',
        'item' => t('You can only add payments to in-progress orders.'),
      ));

  $rules['commerce_pos_payment_validate_add'] = $rule_set;

  //Now a component for removing a payment, with same $variables and $provides.
  $rule_set = rules_rule_set($variables, $provides);
  $rule_set->label = t('Attempt to remove a payment in the POS');

  $rule = rule();
  $rule->label = t('Disallow removing payment transaction when status is not in progress');
  $rule->active = TRUE;
  $rule->condition(rules_condition('data_is', array(
        'data:select' => 'commerce-order:status',
        'op' => '==',
        'value' => 'pos_in_progress',
        ))->negate())
       ->action('list_add', array(
        'list:select' => 'errors',
        'item' => t('You can only remove payments from in-progress orders.'),
      ));
  $rules['commerce_pos_payment_validate_remove'] = $rule_set;



  //When the order is paid off, set the status...
  $rule = rules_reaction_rule();
  $rule->label = t('Set order status to completed');
  $rule->active = TRUE;
  $rule->event('commerce_pos_payment_order_paid')
      ->action('commerce_order_update_status', array(
          'commerce_order:select' => 'commerce-order',
          'order_status' => 'pos_completed',
        ));

  $rules['commerce_pos_payment_set_completed'] = $rule;

  //...and print the receipt
  $rule = rules_reaction_rule();
  $rule->label = t('Print receipt');
  $rule->active = TRUE;
  $rule->event('commerce_pos_payment_order_paid')
      ->action('commerce_pos_order_print_receipt', array(
          'commerce_order:select' => 'commerce-order',
        ));

  $rules['commerce_pos_payment_print_receipt'] = $rule;

  return $rules;

}