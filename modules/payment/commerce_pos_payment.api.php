<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Defines payment methods to be used in to Commerce POS.
 *
 *  @return
 *  An array of payment methods, as in hook_commerce_payment_method_info(), with the following
 *  differences.
 *    -Callbacks will be defined to prevent the payment from appearing to site visitors.
 *    - The key 'pos' may be defined to be an array defining the following keys:
 *      - 'callbacks' - an array of callbacks for the POS action. If left blank, default callbacks
 *        will be set to automatically add a completed payment transaction to an order. The only reason
 *        to override these defaults is to add more complex behavior.
 *      - 'button text' - the string that should appear on the button for this payment method.
 *      - 'action text' - the action text to be used for the method's POS command.
 * @see hook_commerce_payment_method_info()
 * @see hook_commerce_pos_action_info()
 * @see hook_commerce_pos_button_info()
 */
function hook_commerce_pos_payment_method_info() {
  return array(
    'commerce_pos_payment_cash' => array(
      'method_id' => 'commerce_pos_payment_cash',
      'title' => t('Cash payment'),
      'description' => t('Cash payment method for the POS'),
      'pos' => array(
        'button text' => t('Cash'),
        'action text' => 'CA',
      ),
    ),
  );
}