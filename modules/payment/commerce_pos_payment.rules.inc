<?php

/**
 * @file
 * Rules integration for POS payments.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_event_info().
 */
function commerce_pos_payment_rules_event_info() {
  // allow use of entity_rules_events_variables() helper function.
  module_load_include('inc', 'entity', 'entity.rules');

  $events['commerce_pos_payment_order_paid'] = array(
    'label' => t('POS Order paid off'),
    'group' => t('Commerce POS Payment'),
    'variables' => array_merge(
      entity_rules_events_variables('commerce_order', t('Order', array(), array('context' => 'a drupal commerce order'))),
      entity_rules_events_variables('commerce_payment_transaction', t('POS payment'), TRUE)
    ),
  );

  return $events;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_pos_payment_rules_action_info() {
  $actions = array();

  $actions['commerce_pos_payment_change'] = array(
    'label' => t('Add change payment to order'),
    'parameter' => array(
      'commerce_order' => array('type' => 'commerce_order', 'label' => t('Order', array(), array('context' => 'a drupal commerce order'))),
      'commerce_payment_transaction' => array(
        'type' => 'commerce_payment_transaction',
        'label' => t('Last completed payment transaction', array(), array('context' => 'last completed')),
      ),
    ),
    'group' => t('Commerce POS Payment'),
    'base' => 'commerce_pos_payment',
    'callbacks' => array(
      'execute' => 'commerce_pos_payment_change',
    ),
  );

  return $actions;
}

/**
 * Execution callback for change calculation
 */
function commerce_pos_payment_change($order, $payment_transaction, $action_settings, $rule_state, $action, $callback_type) {
  $balance = commerce_payment_order_balance($order);
  if (!empty($balance['amount'])) {
    if ($balance['amount'] < 0) {
      $change = commerce_pos_payment_transaction($payment_transaction->payment_method, $order->order_id, $balance);
      commerce_payment_transaction_save($change);
    }
  }
}

/**
 * @}
 */
