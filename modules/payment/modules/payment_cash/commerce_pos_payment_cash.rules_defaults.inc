<?php

/**
 * @file
 * Default rules configuration for cash payments.
 */

function commerce_pos_payment_cash_default_rules_configuration() {

  $rule = rules_reaction_rule();
  $rule->label = t('Calculate change: Cash');
  $rule->active = TRUE;
  $rule->event('commerce_pos_payment_order_paid')
    ->condition('data_is', array(
        'data:select' => 'commerce-payment-transaction:payment-method',
        'op' => '==',
        'value' => 'commerce_pos_payment_cash',
      ))
      ->action('commerce_pos_payment_change', array(
          'commerce_order:select' => 'commerce-order',
          'commerce_payment_transaction:select' => 'commerce-payment-transaction',
        ));

  $rules['commerce_pos_payment_cash_change'] = $rule;

  return $rules;
}