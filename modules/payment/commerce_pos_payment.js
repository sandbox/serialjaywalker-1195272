(function ($) {
  Drupal.behaviors.commercePosPayment = {
      attach: function (context, settings) {
        $('.commerce-pos-payment-transaction-cancel', context).once('remove-payment').mousedown(function(context) {
          var transactionId = $('.commerce-pos-payment-transaction-cancel-id', this).html();
          $('.commerce-pos-input', $(this).parents('form')).val(transactionId);
          $('.commerce-pos-new-action-name', $(this).parents('form')).val('commerce_pos_payment_cancel');
          $('.commerce-pos-ajax-trigger', $(this).parents('form')).click();
        });
        $('.commerce-pos-payment-transaction-cancel-label').once('button')
        .button({'text':false, 'icons':{primary:'ui-icon-trash'}});
        var orderTotal = $('.field-name-commerce-order-total', context);
        if(orderTotal.length) {
          $('.view-commerce-pos-payments').offset({top:orderTotal.offset().top + orderTotal.height()});
        }
      }
  };
})(jQuery);