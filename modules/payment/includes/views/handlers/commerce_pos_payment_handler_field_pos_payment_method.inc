<?php

/**
 * Field handler to translate a payment method ID into its readable form.
 */
class commerce_pos_payment_handler_field_pos_payment_method extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['payment_method'] = 'payment_method';
    $this->additional_fields['amount'] = 'amount';
    $this->additional_fields['status'] = 'status';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $method = $this->get_value($values, 'payment_method');
    $amount = $this->get_value($values, 'amount');
    $status = strtr($this->get_value($values, 'status'), '_', '-');

    $return_val = array();

    if ($payment_method = commerce_payment_method_load($method)) {
      $short_title = $payment_method['short_title'];
      if ($amount >= 0) {
        $return_val['#markup'] = $short_title;
      }
      else {
        $return_val['#markup'] = t('@method change', array('@method' => $short_title));
      }
    }
    else {
      $return_val['#markup'] = t('Unknown');
    }
    $return_val['#prefix'] = '<span class = \'commerce-pos-payment-' . $status .'\'>';
    $return_val['#suffix'] = '</span>';
    return $return_val;
  }
}