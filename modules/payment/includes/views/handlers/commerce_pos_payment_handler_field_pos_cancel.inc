<?php

/**
 * Field handler to allow a payment transaction to be removed.
 */
class commerce_pos_payment_handler_field_pos_cancel extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['transaction_id'] = 'transaction_id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $status = strtr($this->get_value($values, 'status'), '_', '-');
    return array(
      '#prefix' => '<div class = \'commerce-pos-payment-transaction-cancel\'>',
      '#suffix' => '</div>',
      array(
        '#markup' => t('Remove'),
        '#prefix' => '<span class = \'commerce-pos-payment-transaction-cancel-label commerce-pos-payment-' . $status . '\'>',
        '#suffix' => '</span>',
      ),
      array(
      	'#markup' => $this->get_value($values, 'transaction_id'),
      	'#prefix' => '<span class = \'commerce-pos-payment-transaction-cancel-id\'>',
      	'#suffix' => '</span>',
      ),
    );
  }
}
