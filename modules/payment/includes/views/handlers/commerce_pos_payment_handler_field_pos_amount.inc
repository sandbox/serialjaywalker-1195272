<?php

/**
 * @file
 * Contains the basic amount field handler.
 */

/**
 * Field handler to allow rendering of the amount using currency formatting.
 */
class commerce_pos_payment_handler_field_pos_amount extends views_handler_field {
  function init(&$view, &$options) {
    parent::init($view, $options);
    $this->additional_fields['amount'] = 'amount';
    $this->additional_fields['currency_code'] = 'currency_code';
    $this->additional_fields['status'] = 'status';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $amount = $this->get_value($values, 'amount');
    $status = strtr($this->get_value($values, 'status'), '_', '-');
    $currency_code = $this->get_value($values, 'currency_code');
    $change = ($amount < 0);
    $attributes = array(
      'class' => array(
        'commerce-pos-payment-amount',
        'commerce-pos-payment-' . $status,
      ),
    );

    if ($change) {
      $attributes['class'][] = 'commerce-pos-payment-change';
    }

    return array(
      '#markup' => commerce_currency_format(abs($amount), $currency_code),
      '#prefix' => '<span ' . drupal_attributes($attributes) . ' >',
      '#suffix' => '</span>',
    );
  }
}
