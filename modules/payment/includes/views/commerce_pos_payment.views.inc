<?php

/**
 * Add views handlers for removing payments and for indicating that a payment
 * is to be considered change, i.e. payment returned to the customer
 */

/**
 * Implements hook_views_data()
 */
function commerce_pos_payment_views_data_alter(&$data) {
  $data['commerce_payment_transaction']['pos_payment_method'] =  array(
    'field' => array(
      'title' => t('POS Method'),
      'help' => t('Indicates the payment method, and whether the payment is change given.'),
      'handler' => 'commerce_pos_payment_handler_field_pos_payment_method',
    ),
  );
  $data['commerce_payment_transaction']['pos_cancel'] =  array(
    'field' => array(
      'title' => t('POS Cancel'),
      'help' => t('Allows for easy cancellation of payments in the POS.'),
      'handler' => 'commerce_pos_payment_handler_field_pos_cancel',
    ),
  );
  $data['commerce_payment_transaction']['pos_amount'] =  array(
    'field' => array(
      'title' => t('POS Amount'),
      'help' => t('Displays the amount, but always as a positive number.'),
      'handler' => 'commerce_pos_payment_handler_field_pos_amount',
    ),
  );
}