<?php

/**
 * @file
 *  Contains default views for the payment module.
 */

/**
 * Implements hook_views_default_views().
 */
function commerce_pos_payment_views_default_views() {
  $views = array();
  $view = new view;
  $view->name = 'commerce_pos_payments';
  $view->description = '';
  $view->tag = 'commerce_pos';
  $view->base_table = 'commerce_payment_transaction';
  $view->human_name = 'POS Payments';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
  'payment_method' => 'payment_method',
  'amount' => 'amount',
  'status' => 'status',
  'pos_cancel' => 'pos_cancel',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
  'payment_method' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'amount' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'status' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'pos_cancel' => array(
    'align' => '',
    'separator' => '',
  ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Footer: Commerce Payment Transaction: Totals */
  $handler->display->display_options['footer']['totals']['id'] = 'totals';
  $handler->display->display_options['footer']['totals']['table'] = 'commerce_payment_transaction';
  $handler->display->display_options['footer']['totals']['field'] = 'totals';
  $handler->display->display_options['footer']['totals']['empty'] = FALSE;
  $handler->display->display_options['footer']['totals']['add_payment_form'] = 0;
  /* Field: Commerce Payment Transaction: POS Cancel */
  $handler->display->display_options['fields']['pos_cancel']['id'] = 'pos_cancel';
  $handler->display->display_options['fields']['pos_cancel']['table'] = 'commerce_payment_transaction';
  $handler->display->display_options['fields']['pos_cancel']['field'] = 'pos_cancel';
  $handler->display->display_options['fields']['pos_cancel']['label'] = '';
  $handler->display->display_options['fields']['pos_cancel']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['pos_cancel']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['pos_cancel']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['pos_cancel']['alter']['external'] = 0;
  $handler->display->display_options['fields']['pos_cancel']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['pos_cancel']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['pos_cancel']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['pos_cancel']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['pos_cancel']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['pos_cancel']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['pos_cancel']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['pos_cancel']['alter']['html'] = 0;
  $handler->display->display_options['fields']['pos_cancel']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['pos_cancel']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['pos_cancel']['hide_empty'] = 0;
  $handler->display->display_options['fields']['pos_cancel']['empty_zero'] = 0;
  $handler->display->display_options['fields']['pos_cancel']['hide_alter_empty'] = 0;
  /* Field: Commerce Payment Transaction: POS Method */
  $handler->display->display_options['fields']['pos_payment_method']['id'] = 'pos_payment_method';
  $handler->display->display_options['fields']['pos_payment_method']['table'] = 'commerce_payment_transaction';
  $handler->display->display_options['fields']['pos_payment_method']['field'] = 'pos_payment_method';
  $handler->display->display_options['fields']['pos_payment_method']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['external'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['html'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['pos_payment_method']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['pos_payment_method']['hide_empty'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['empty_zero'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['hide_alter_empty'] = 0;
  /* Field: Commerce Payment Transaction: POS Amount */
  $handler->display->display_options['fields']['pos_amount']['id'] = 'pos_amount';
  $handler->display->display_options['fields']['pos_amount']['table'] = 'commerce_payment_transaction';
  $handler->display->display_options['fields']['pos_amount']['field'] = 'pos_amount';
  $handler->display->display_options['fields']['pos_amount']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['external'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['pos_amount']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['pos_amount']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['html'] = 0;
  $handler->display->display_options['fields']['pos_amount']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['pos_amount']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['pos_amount']['hide_empty'] = 0;
  $handler->display->display_options['fields']['pos_amount']['empty_zero'] = 0;
  $handler->display->display_options['fields']['pos_amount']['hide_alter_empty'] = 0;
  /* Contextual filter: Commerce Payment Transaction: Order ID */
  $handler->display->display_options['arguments']['order_id']['id'] = 'order_id';
  $handler->display->display_options['arguments']['order_id']['table'] = 'commerce_payment_transaction';
  $handler->display->display_options['arguments']['order_id']['field'] = 'order_id';
  $handler->display->display_options['arguments']['order_id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['order_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['order_id']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['order_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['order_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['order_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['order_id']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['order_id']['not'] = 0;

  /* Display: Screen */
  $handler = $view->new_display('block', 'Screen', 'screen');

  /* Display: Receipt */
  $handler = $view->new_display('block', 'Receipt', 'receipt');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Commerce Payment Transaction: POS Method */
  $handler->display->display_options['fields']['pos_payment_method']['id'] = 'pos_payment_method';
  $handler->display->display_options['fields']['pos_payment_method']['table'] = 'commerce_payment_transaction';
  $handler->display->display_options['fields']['pos_payment_method']['field'] = 'pos_payment_method';
  $handler->display->display_options['fields']['pos_payment_method']['label'] = '';
  $handler->display->display_options['fields']['pos_payment_method']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['external'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['alter']['html'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['pos_payment_method']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['pos_payment_method']['hide_empty'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['empty_zero'] = 0;
  $handler->display->display_options['fields']['pos_payment_method']['hide_alter_empty'] = 0;
  /* Field: Commerce Payment Transaction: POS Amount */
  $handler->display->display_options['fields']['pos_amount']['id'] = 'pos_amount';
  $handler->display->display_options['fields']['pos_amount']['table'] = 'commerce_payment_transaction';
  $handler->display->display_options['fields']['pos_amount']['field'] = 'pos_amount';
  $handler->display->display_options['fields']['pos_amount']['label'] = '';
  $handler->display->display_options['fields']['pos_amount']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['external'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['pos_amount']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['pos_amount']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['pos_amount']['alter']['html'] = 0;
  $handler->display->display_options['fields']['pos_amount']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['pos_amount']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['pos_amount']['hide_empty'] = 0;
  $handler->display->display_options['fields']['pos_amount']['empty_zero'] = 0;
  $handler->display->display_options['fields']['pos_amount']['hide_alter_empty'] = 0;

  $views['commerce_pos_payments'] = $view;

  return $views;

}