<?php

/**
 * @file
 * Rules integration for orders in commerce POS
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_event_info().
 */
function commerce_pos_order_rules_event_info() {
  $events = array();

  $events['commerce_pos_order_set_status'] = array(
    'label' => t('Attempt to change the order status in the POS.'),
    'group' => t('Commerce POS Order'),
    'variables' => array(
    	'commerce_order' => array(
        'label' => t('Order'),
        'type' => 'commerce_order',
        'skip save' => TRUE,
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  $events['commerce_pos_modify_order_validate'] = array(
    'label' => t('Attempt to modify an order in the POS.'),
    'group' => t('Commerce POS Order'),
    'variables' => array(
      'commerce_order' => array(
        'label' => t('Order to be modified'),
        'type' => 'commerce_order',
        'skip save' => TRUE,
      ),
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  return $events;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_pos_order_rules_action_info() {
  $actions = array();

  $actions['commerce_pos_order_print_receipt'] = array(
    'label' => t('Print receipt'),
    'group' => t('Commerce POS Order'),
    'parameter' => array(
      'commerce_order' => array('type' => 'commerce_order', 'label' => t('Order', array(), array('context' => 'a drupal commerce order'))),
    ),
    'callbacks' => array( 'execute' => 'commerce_pos_order_rules_print_receipt'),
  );

  $actions['commerce_pos_order_print_empty_receipt'] = array(
    'label' => t('Print empty receipt'),
    'group' => t('Commerce POS Order'),
    'parameter' => array(
      'commerce_order' => array('type' => 'commerce_order', 'label' => t('Order', array(), array('context' => 'a drupal commerce order'))),
    ),
    'callbacks' => array( 'execute' => 'commerce_pos_order_rules_print_empty_receipt'),
  );

    $actions['commerce_pos_order_set_error'] = array(
    'label' => t('Set error'),
    'group' => t('Commerce POS Order'),
    'parameter' => array(
      'commerce_order' => array('type' => 'commerce_order', 'label' => t('Order', array(), array('context' => 'a drupal commerce order'))),
      'message' => array('type' => 'text', 'label' => t('Message'), ),
    ),
    'callbacks' => array( 'execute' => 'commerce_pos_order_rules_set_error'),
  );

  $actions['commerce_pos_order_update_status'] = array(
    'label' => t('Update the order status without saving'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order to update'),
      ),
      'order_status' => array(
        'type' => 'text',
        'label' => t('Order status'),
        'options list' => 'commerce_order_status_options_list',
      ),
    ),
    'group' => t('Commerce Order'),
    'callbacks' => array(
      'execute' => 'commerce_pos_order_rules_update_status',
    ),
  );

  $actions['commerce_pos_order_update_total'] = array(
    'label' => t('Update order total'),
    'group' => t('Commerce POS Order'),
    'parameter' => array(
      'commerce_order' => array('type' => 'commerce_order', 'label' => t('Order', array(), array('context' => 'a drupal commerce order'))),
    ),
    'callbacks' => array( 'execute' => 'commerce_pos_order_rules_update_total'),
  );

  return $actions;
}

/**
 * Rules action: updates an order's status using the Order API.
 */
function commerce_pos_order_rules_update_status($order, $name) {
  commerce_order_status_update($order, $name, TRUE, TRUE, t('Order status updated via Rules.'));
}


/**
 * Execute callback for printing a receipt.
 */
function commerce_pos_order_rules_print_receipt($order, $action_settings, $rule_state, $action, $callback_type) {
  $order->data['print_receipt'] = REQUEST_TIME;
}

/**
 * Execute callback for printing an empty receipt.
 */
function commerce_pos_order_rules_print_empty_receipt($order, $action_settings, $rule_state, $action, $callback_type) {
  $order->data['print_empty_receipt'] = REQUEST_TIME;
}

/**
 * Execute callback for setting an error
 */
function commerce_pos_order_rules_set_error($order, $message, $action_settings, $rule_state, $action, $callback_type) {
  $order->error = check_plain($message);
}

/**
 * Execute callback for updating the order total.
 */
function commerce_pos_order_rules_update_total($order, $action_settings, $rule_state, $action, $callback_type) {
  commerce_order_calculate_total($order);
}
 /*
  * @}
  */
