(function ($) {

  Drupal.behaviors.commercePosOrder = {
      attach: function (context, settings) {
        //Bind action to order status radios.
        $('.commerce-pos-order-status label').once('order-status-set')
        .bind('click', function() {
          $('.commerce-pos-new-action-name').val('commerce_pos_order_set_status');
        });
        var trigger = $('.commerce-pos-ajax-trigger');
        $('.commerce-pos-order-status').once('trigger').change(function(){
          trigger.click();
        });
        $('.commerce-pos-order-status', context).once('order-status').buttonset();

        //Get rid of link in order number: TODO: remove URL in preprocessor.
        $('.entity-commerce-order h2 a').once('no-link').click(function(e) {e.preventDefault();});
        //Change behavior of clear button to prevent accidental clearing of in progress orders
        $('.commerce-pos-button-commerce-pos-clear', context).unbind('mousedown')
          .mousedown(function() {
            var orderStatus = $("input[name='order[order_status]']:checked");
            if (!orderStatus.length || orderStatus.val() != 'pos_in_progress' || confirm(Drupal.t('Clear in-progress order?'))) {
              var action = $('.commerce-pos-button-action', $(this)).html();
              if(action && action.length > 0) {
                $('.commerce-pos-new-action-name').val(action);
              }
              $('.commerce-pos-ajax-trigger').click();
            }
          });        
      }
  };

})(jQuery);
