<?php

/**
 * @file
 * Default rules configuration for adding and removing line items.
 */

function commerce_pos_order_default_rules_configuration() {
  $rule = rules_reaction_rule();
  $rule->label = t('Disallow changing status to completed with nonzero balance');
  $rule->active = TRUE;

  $rule->event('commerce_pos_order_set_status')
    ->condition('data_is', array(
      'data:select' => 'commerce-order:status',
      'op' => '==',
      'value' => 'pos_completed',
      ))
    ->condition(rules_condition('commerce_payment_order_balance_comparison', array(
      'commerce_order:select' => 'commerce_order',
      'op' => '=',
      'value' => 0,
      ))->negate())
    ->action('commerce_pos_order_set_error', array(
        'commerce_order:select' => 'commerce-order',
        'message' => t('You can only set the status to completed if the balance is zero.' ),
      ));

  $rules['commerce_pos_order_change_status'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = t('Disallow modifying order when status not in progress');
  $rule->active = TRUE;

  $rule->event('commerce_pos_modify_order_validate')
    ->condition(rules_condition('data_is', array(
      'data:select' => 'commerce-order:status',
      'op' => '==',
      'value' => 'pos_in_progress',
      ))->negate())
    ->action('commerce_pos_order_set_error', array(
        'commerce_order:select' => 'commerce-order',
        'message' => t('You can only change in progress orders' ),
      ));

  $rules['commerce_pos_order_only_change_in_progress'] = $rule;

  return $rules;

}