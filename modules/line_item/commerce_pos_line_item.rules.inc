<?php

/**
 * @file
 * Rules integration for adding and removing line items in the POS
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_event_info().
 */
function commerce_pos_line_item_rules_event_info() {
  $events = array();

  $events['commerce_pos_line_item_add'] = array(
    'label' => t('Attempt to add a line item in the POS.'),
    'group' => t('Commerce POS Line Item'),
    'variables' => array_merge(
  entity_rules_events_variables('commerce_order', t('Order', array(), array('context' => 'a drupal commerce order'))),
  entity_rules_events_variables('commerce_line_item', t('Line item to be added'), TRUE)
  ),
    'access callback' => 'commerce_order_rules_access',
  );

  $events['commerce_pos_line_item_remove'] = array(
    'label' => t('Attempt to remove a line item in the POS.'),
    'group' => t('Commerce POS Line Item'),
    'variables' => array_merge(
  entity_rules_events_variables('commerce_order', t('Order', array(), array('context' => 'a drupal commerce order'))),
  entity_rules_events_variables('commerce_line_item', t('Line item to add'), TRUE)
  ),
    'access callback' => 'commerce_order_rules_access',
  );

 $events['commerce_pos_line_item_on_add'] = array(
    'label' => t('On adding a line item in the POS.'),
    'group' => t('Commerce POS Line Item'),
    'variables' => array_merge(
  entity_rules_events_variables('commerce_line_item', t('Line item to add'), TRUE),
  entity_rules_events_variables('commerce_order', t('Order', array(), array('context' => 'a drupal commerce order')))
  ),    'access callback' => 'commerce_order_rules_access',
  );

 $events['commerce_pos_line_item_after_add'] = array(
    'label' => t('After adding a line item in the POS.'),
    'group' => t('Commerce POS Line Item'),
    'variables' => array_merge(
  entity_rules_events_variables('commerce_line_item', t('Line item to add'), TRUE),
  entity_rules_events_variables('commerce_order', t('Order', array(), array('context' => 'a drupal commerce order')))
  ),    'access callback' => 'commerce_order_rules_access',
  );

   $events['commerce_pos_line_item_after_remove'] = array(
    'label' => t('After removing a line item in the POS.'),
    'group' => t('Commerce POS Line Item'),
    'variables' => array_merge(
  entity_rules_events_variables('commerce_line_item', t('Line item to be removed'), TRUE),
  entity_rules_events_variables('commerce_order', t('Order', array(), array('context' => 'a drupal commerce order')))
  ),    'access callback' => 'commerce_order_rules_access',
  );

  return $events;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_pos_line_item_rules_action_info() {
  $actions = array();

  $actions['commerce_pos_line_item_error'] = array(
    'label' => t('Set error'),
    'group' => t('Commerce POS Line Item'),
    'parameter' => array(
      'commerce_order' => array('type' => 'commerce_order', 'label' => t('Order', array(), array('context' => 'a drupal commerce order'))),
      'commerce_line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Line Item', array(), array('context' => 'a drupal commerce line item')),
      ),
      'message' => array('type' => 'text', 'label' => t('Message'), ),
    ),
    'callbacks' => array( 'execute' => 'commerce_pos_line_item_set_error'),
  );

    $actions['commerce_pos_line_item_clear_components'] = array(
    'label' => t('Clear price components'),
    'group' => t('Commerce POS Line Item'),
    'parameter' => array(
      'commerce_line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Line Item', array(), array('context' => 'a drupal commerce line item')),
      ),
    ),
    'callbacks' => array( 'execute' => 'commerce_pos_line_item_rules_clear_components'),
  );

  return $actions;
}

/**
 * Execute callback for setting an error
 */
function commerce_pos_line_item_set_error($order, $line_item, $message, $action_settings, $rule_state, $action, $callback_type) {
  $order->error = check_plain($message);
}

/**
 * Execute callback for setting an error
 */
function commerce_pos_line_item_rules_clear_components($line_item, $action_settings, $rule_state, $action, $callback_type) {
  $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  //reset the price array to just include the base price.
  $price = commerce_price_wrapper_value($wrapper, 'commerce_unit_price');

  foreach ((array) $price['data']['components'] as $key => $component) {
    if ($component['name'] != 'base_price') {
      unset($price['data']['components'][$key]);
    }
  }
  $total = commerce_price_component_total($price);
  $price['amount'] = $total['amount'];
  $wrapper->commerce_unit_price = $price;

}
 /*
  * @}
  */
