<?php

/**
 * Field handler to allow a line item to be selected.
 */
class commerce_pos_line_item_handler_field_select extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['line_item_id'] = 'line_item_id';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    return array(
      '#markup' => $this->get_value($values, 'line_item_id'),
      '#prefix' => '<div class = \'commerce-pos-line-item-selector\'>',
      '#suffix' => '</div>',
    );
  }
}
