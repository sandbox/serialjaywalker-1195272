<?php

/**
 * Add a views handler to allow line item selection in the POS.
 */

/**
 * Implements hook_views_data()
 */
function commerce_pos_line_item_views_data_alter(&$data) {
  $data['commerce_line_item']['pos_selector'] =  array(
    'field' => array(
      'title' => t('POS Selector'),
      'help' => t('Makes line items selectable in the POS.'),
      'handler' => 'commerce_pos_line_item_handler_field_select',
    ),
  );
}