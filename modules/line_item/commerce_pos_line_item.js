
(function ($) {

	Drupal.behaviors.commercePosLineItem = {
			attach: function (context, settings) {
	  $('body').once('arrows').bind('keypress', function(event) {	  
			switch(event.keyCode) {
			case 38:
				//Up key.
				event.preventDefault();
				$('.commerce-pos-order');
				lineItemUp($('.commerce-pos-order'));
				break;
			case 40:
				//Down arrow key.
				event.preventDefault();
				lineItemDown($('.commerce-pos-order'));
				break;
			}
		});
		$('.field-name-commerce-line-items table').selectable({
		  stop:function() {setSelectedLineItems($(this).parents('form'));},
		  filter:'tr'
	  });
		//Select a clicked line item.
		var scrollLineItems = function(lineItemId, context) {
			var row = $('.ui-selected', context);
			var lineItems = $('.field-name-commerce-line-items', context);
			//Scroll to the newly selected line item.
			var selectedBottom = row.offset().top + row.height();
			if(row.offset().top < lineItems.offset().top) {
				lineItems.animate({
					scrollTop: lineItems.scrollTop() - lineItems.offset().top + row.offset().top
				}, 50);
			}
			else if(selectedBottom > lineItems.offset().top + lineItems.height()) {
				lineItems.animate({
					scrollTop: lineItems.scrollTop() + selectedBottom - lineItems.offset().top - lineItems.height()
				}, 50);
			}
		};
		var lineItemUp = function(context) {
		  var firstSelected = $('.ui-selected', context);
		  if(firstSelected.prev().length) {
		    $('.ui-selected', context).each(function() {$(this).removeClass('ui-selected');});
		    firstSelected.prev().addClass('ui-selected');
		    setSelectedLineItems(context);
		    scrollLineItems(context);
		  }
		};
		var lineItemDown = function(context) {
      var lastSelected = $('.ui-selected', context).last();
      if(lastSelected.next().length) {
        $('.ui-selected', context).removeClass('ui-selected');
        lastSelected.next().addClass('ui-selected');
        setSelectedLineItems(context);
        scrollLineItems(context);
      }
		};
		var setSelectedLineItems = function(context) {
      var selector = $('.commerce-line-items-selected-ids', context).val([]);
      var values = [];
      $('.ui-selected', context).each(function() {
        var lineItemId = $('.commerce-pos-line-item-selector', this).html();        
        values.push(lineItemId);
      });
      selector.val(values);
		};
		//Select the last line item upon loading.
		var lineItems = $('.field-name-commerce-line-items', context);
		var lastLineItem = $('.ui-selectee', lineItems).eq(-1);
		if(lastLineItem.length) {
		  lastLineItem.addClass('ui-selected');
		  setSelectedLineItems(context);
		  scrollLineItems(lineItems);
		}
		//Now set up the up and down arrows.
		var buttonUp = $('.commerce-pos-line-item-up', context); 
		buttonUp.once('button').button({text:false, icons:{primary:'ui-icon-triangle-1-n'}});
		buttonUp.once('button-up').button('option', 'text', false);
		buttonUp.once('icons')
		  .button('option', 'icons', {'primary': 'ui-icon-circle-arrow-n', 'secondary': ''});
		var buttonDown = $('.commerce-pos-line-item-down', context); 
    buttonDown.once('button-down').button({text:false, icons:{primary:'ui-icon-triangle-1-n'}});
    buttonDown.once('text').button('option', 'text', false);
    buttonDown.once('icons').button('option', 'icons', {'primary': 'ui-icon-circle-arrow-s', 'secondary': ''});
    
    buttonUp.once('line-item-up').click(function() {lineItemUp(context);});
    buttonDown.once('line-item-down').click(function() {lineItemDown(context);});
	}
};

})(jQuery);