<?php

/**
 * @file
 * Default rules configuration for adding and removing line items.
 */

function commerce_pos_line_item_default_rules_configuration() {
  $rule = rules_reaction_rule();
  $rule->label = t('Disallow adding line items when status is not pending');
  $rule->active = TRUE;

  $rule->event('commerce_pos_line_item_add')
    ->condition(rules_condition('data_is', array(
        'data:select' => 'commerce-order:status',
        'op' => '==',
        'value' => 'pos_in_progress',
        ))->negate())
    ->action('commerce_pos_line_item_error', array(
        'commerce_order:select' => 'commerce-order',
        'commerce_line_item:select' => 'commerce-line-item',
        'message' => t('You can only add line items to in-progress orders.' ),
      ));

  $rules['commerce_pos_line_item_add_status'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = t('Disallow removing line items when status is not pending');
  $rule->active = TRUE;
  $rule->event('commerce_pos_line_item_remove')
    ->condition(rules_condition('data_is', array(
        'data:select' => 'commerce-order:status',
        'op' => '==',
        'value' => 'pos_in_progress',
        ))->negate())
    ->action('commerce_pos_line_item_error', array(
        'commerce_order:select' => 'commerce-order',
        'commerce_line_item:select' => 'commerce-line-item',
        'message' => t('You can only remove line items from in-progress orders.'),
      ));

  $rules['commerce_pos_line_item_remove_status'] = $rule;

  $rule = rules_reaction_rule();
  $rule->label = t('Calculate line item price when adding to order.');
  $rule->active = TRUE;
  $rule->event('commerce_pos_line_item_on_add')
    ->action('component_commerce_pos_line_item_calculate_price', array(
    'commerce_line_item:select' => 'commerce-line-item',
    'commerce_order:select' => 'commerce-order'));
  $rules['commerce_pos_line_item_calculate_on_add'] = $rule;

  $component = rules_rule_set(commerce_pos_line_item_component_variables());
  $component->label = t('Calculating the unit price of a line item at the POS.');
  $action = rules_action('commerce_pos_line_item_clear_components',
    array('commerce_line_item:select' => 'commerce-line-item'));
  $action->weight = -20;
  $component->action($action);

  $rules['commerce_pos_line_item_calculate_price'] = $component;

  return $rules;

}

/**
 * Returns an array of variables for POS price calculation components.
 */
function commerce_pos_line_item_component_variables() {
  return array(
    'commerce_line_item' => array(
      'type' => 'commerce_line_item',
      'label' => t('Line item'),
    ),
    'commerce_order' => array(
      'type' => 'commerce_order',
      'label' => t('Order'),
    ),
  );
}