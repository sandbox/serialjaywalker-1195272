<?php

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_pos_quantity_profile_default_rules_configuration() {
  $rules = array();

  // Loop over every quantity profile looking for those having a rules component.
  foreach (commerce_pos_quantity_profile_info() as $name => $profile) {
    if (!empty($profile['rules component'])) {
      // Create a new rules component.
      $rule = rule(array('commerce_product' => array('type' => 'commerce_product', 'label' => t('Product'))));

      $rule->label = t('Set profile to @title', array('@title' => $profile['title']));

      // Add the action to set the profile.
      $rule
        ->action('commerce_pos_quantity_profile_set', array(
          'commerce_product:select' => 'commerce-product',
          'quantity_profile_name' => $name,
        ));

      $rules['commerce_pos_quantity_profile_' . $name] = $rule;
    }
  }
  return $rules;
}