
(function($) {

  Drupal.behaviors.commercePosQuantityProfile = {
      attach:function(context, settings) {
        $('.entity-commerce-product.commerce-pos-quantity-profile-weight-pound')
          .once('icon').button('option', 'icons', {primary:'ui-icon-arrowthick-1-s', secondary:null});
      }
  };

})(jQuery);