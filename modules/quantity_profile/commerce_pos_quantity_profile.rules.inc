<?php

/**
 * @file
 * Rules integration for quantity profiles.
 *
 * @addtogroup rules
 * @{
 */


/**
 * Implements hook_rules_action_info().
 */
function commerce_pos_quantity_profile_rules_action_info() {
  $actions = array();

    $actions['commerce_pos_quantity_profile_set'] = array(
      'label' => t('Set profile'),
      'parameter' => array(
        'commerce_product' => array(
          'type' => 'commerce_product',
          'label' => t('Product'),
        ),
        'quantity_profile_name' => array(
          'type' => 'text',
          'label' => t('Quantity profile'),
          'options list' => 'commerce_pos_quantity_profile_titles',
        ),
      ),
      'group' => t('Commerce POS Quantity Profile'),
      'callbacks' => array(
        'execute' => 'commerce_pos_quantity_profile_rules_set',
      ),
    );

  return $actions;
}

/**
 * Rules action: set the quantity profile for this product.
 */
function commerce_pos_quantity_profile_rules_set($product, $name) {
  $product->quantity_profile = $name;
}

/**
 * @}
 */
