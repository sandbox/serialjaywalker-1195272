
(function($) {

  Drupal.behaviors.commercePosProduct = {
      attach:function(context, settings) {
        $('.commerce-pos-product-add', context).once('product-add-button').mousedown(function() {
          var sku = $('.sku', this).html();
          var posInput = $('.commerce-pos-input');
          var currentText = posInput.val();
          if (currentText == '' || currentText.substring(currentText.length - 1) == '*') {
            posInput.val(currentText + sku);
          }
          else {
            posInput.val(currentText + '*' + sku);
          }
          $('.commerce-pos-ajax-trigger').click();
        }).button();
        $('div.commerce-pos-category-select', context).once('buttonset').buttonset();

        var titleSearch = $('.commerce-pos-product-title-search', context); 
        titleSearch.once('search-keyup').keyup(function(event) {
          oldTitle = $('.commerce-pos-product-title-search', context).val();
          setTimeout(function() {
            newTitle = $('.commerce-pos-product-title-search', context).val();
            if (newTitle == oldTitle)
            setSelectorProducts();
          }, 150);
        });
        titleSearch.once('search-no-enter').keydown(function(event) {
          if (event.which == 13) {
            event.preventDefault();
          }
          });
        
        $('input.commerce-pos-category-select', context).once('clear-search').change(function() {
          $('.commerce-pos-product-title-search', context).val('').focus();
          setSelectorProducts();
        });

        var setSelectorProducts = function() {
          var tid = $('.commerce-pos-category-select:input:checked').val();
          var title = $('.commerce-pos-product-title-search').val();
          
          if(title !=setSelectorProducts.title || tid != setSelectorProducts.tid) {
            setSelectorProducts.tid = tid;
            setSelectorProducts.title = title;
            
            var path = Drupal.settings.basePath + 'commerce_pos/product_search/' + tid + '/0/10';
            if(title.length) {
              path = path + '/' + title;
            }

            $.getJSON(path, function(result) {
              title = $('.commerce-pos-product-title-search', context).val();
              if (title == result.title) {
                var wrapper = $('#commerce-pos-product-select-products');
                if (!result.products) {
                  result.products = '';
                }
                Drupal.detachBehaviors(wrapper);       
                wrapper.html(result.products);
                Drupal.attachBehaviors(wrapper);
                //Now get products outside of the category if there's still space.
                if(title != '') {
                  var resultCount = $('.commerce-pos-product-add', wrapper).length;
                  //TODO: Make the number of results displayed customizable
                  if(resultCount < 10) {
                    path = Drupal.settings.basePath + 'commerce_pos/product_search/' + tid + '/1/' + (10-resultCount) + '/' + title;
                    $.getJSON(path, function(result) {
                      if (!result.products) {
                        result.products = '';
                      }
                      if (title == result.title && $('.commerce-pos-product-select-products-negate', wrapper).length == 0) {
                        $(result.products).appendTo(wrapper);
                        var newProducts = $('.commerce-pos-product-select-products-negate', wrapper);
                        Drupal.attachBehaviors(newProducts); 
                      }
                    });
                  }
                }
              }
            });
          }
        };
      }
  };

})(jQuery);