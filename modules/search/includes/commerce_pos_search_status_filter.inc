<?php

/**
 * Data alteration callback that filters out disabled Commerce entities.
 */
class CommercePosSearchStatusFilter extends SearchApiAbstractAlterCallback {

  /**
   * @param SearchApiIndex $index
   * @return bool
   */
  public function supportsIndex(SearchApiIndex $index) {
    return in_array($index->item_type, array('commerce_product', 'commerce_customer_profile'));
  }

  /**
   * Exclude items that doesn't have a product reference field attached to
   * the bundle.
   */
  public function alterItems(array &$items) {
    foreach ($items as $id => $item) {
      if (empty($item->status)) {
        unset($items[$id]);
      }
    }
  }

}
