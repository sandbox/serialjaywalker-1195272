<?php

/**
 * @file
 * The facetapi_links and facetapi_checkbox_links widget plugin classes.
 */

/**
 * Widget that renders facets as a list of clickable links.
 *
 * Links make it easy for users to narrow down their search results by clicking
 * on them. The render arrays use theme_item_list() to generate the HTML markup.
 */
class CommercePOSSearchWidgetRadios extends FacetapiWidget {

  /**
   * Overrides FacetapiWidget::__construct().
   */
  public function __construct($id, array $realm, FacetapiFacet $facet, stdClass $settings) {
    parent::__construct($id, $realm, $facet, $settings);
//    $this->jsSettings['limit'] = $this->settings->settings['soft_limit'];
    $this->key = $facet['name'];
  }

  /**
   * Implements FacetapiWidget::execute().
   *
   * Transforms the render array into something that can be themed by
   * theme_item_list().
   *
   * @see FacetapiWidgetLinks::setThemeHooks()
   * @see FacetapiWidgetLinks::buildListItems()
   */
  public function execute() {
    $element = &$this->build[$this->facet['field alias']];

    // Sets each item's theme hook, builds item list.
    //$this->setThemeHooks($element);
    $element = array(
      '#type' => 'radios',
      '#options' => $this->getOptions($element),
      '#attributes' => $this->build['#attributes'],
      '#attached' => array(
        'js' => array(drupal_get_path('module', 'commerce_pos_search') . '/commerce_pos_search.js' => array()),
      ),
      'clear' => array(
        '#type' => 'markup',
        '#prefix' => '<a class = \'commerce-pos-search-radios-clear\'>',
        '#suffix' => '</a>',
        '#markup' => t('Clear'),
        '#weight' => 100,
      ),
    );
    //If there's only one option, make sure it's selected.
    if (count($element['#options']) == 1) {
      $element['#default_value'] = reset(array_keys($element['#options']));
    }
    unset($element['#attributes']['id']);
    //$element = form_process_radios($element);
  }

  /**
   * Get the options for the radio element.
   *
   * @param array $build
   *   The items in the facet's render array being transformed.
   *
   * @return array
   *   The "items" parameter for theme_item_list().
   */
  function getOptions($build) {
    //$settings = $this->settings->settings;
    $options = array();
    // Initializes links attributes, adds rel="nofollow" if configured.
    $attributes = array('class' => array());
    $items = array();
    foreach ($build as $value => $item) {
      // Adds the facetapi-zero-results class to items that have no results.
      if ($item['#count']) {
        $options[$item['#indexed_value']] = $item['#markup'];
      }
    }

    return $options;
  }

}


