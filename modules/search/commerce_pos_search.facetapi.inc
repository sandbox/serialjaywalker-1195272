<?php

/**
 * @file
 * Common functions for search features integrated with the POS.
 */

/**
 * Implements hook_facetapi_realm_info().
 */
function commerce_pos_search_facetapi_realm_info() {
 return array(
   'commerce_pos_search' => array(
     'label' => t('POS Search'),
     'sortable' => FALSE,
     'weight' => -10,
     'default widget' => 'commerce_pos_search_radios',
     'element type' => 'form elements',
     'settings callback' => 'commerce_pos_search_realm_settings',
     'description' => t('The <em>POS</em> realm displays facets in the Commerce POS panels and automatically updates via AJAX.'),
   ),
 );
}

/**
 * Settings callback for the commerce_pos realm.
 */
function commerce_pos_search_realm_settings() {
 
}

/**
 * Implements hook_facetapi_widgets().
 */
function commerce_pos_search_facetapi_widgets() {
  return array(
      'commerce_pos_search_radios' => array(
          'handler' => array(
              'label' => t('Commerce POS Search Radios'),
              'class' => 'CommercePOSSearchWidgetRadios',
              'query types' => array('term'),
              'requirements' => array(
                  'facetapi_requirement_realm_property' => array('element type' => 'form elements')
              ),
          ),
      ),
  );
}
