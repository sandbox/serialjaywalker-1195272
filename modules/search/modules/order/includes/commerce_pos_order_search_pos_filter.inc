<?php

/**
 * Data alteration callback that filters out non-POS Commerce orders.
 */
class CommercePosOrderSearchPosFilter extends SearchApiAbstractAlterCallback {

  /**
   * @param SearchApiIndex $index
   * @return bool
   */
  public function supportsIndex(SearchApiIndex $index) {
    return in_array($index->item_type, array('commerce_order'));
  }

  /**
   * Exclude items that doesn't have a product reference field attached to
   * the bundle.
   */
  public function alterItems(array &$items) {
    $statuses = commerce_pos_order_statuses();
    foreach ($items as $id => $item) {
      if (empty($statuses[$item->status])) {
        unset($items[$id]);
      }
    }
  }
}
