
(function($) {

  Drupal.behaviors.commercePosOrderSearch = {
      attach:function(context, settings) {
    	//Button behaviors.
        $('.commerce-pos-search-results-container .entity-commerce-order', context)
          .once('order-load-button').mousedown(function() {
          var orderNumber = $('.commerce-pos-order-search-order-number', this).html();
          var posInput = $('.commerce-pos-input');
          posInput.val(orderNumber);
          $('.commerce-pos-new-action-name').val('commerce_pos_order_load');
          $('.commerce-pos-ajax-trigger').click();
        }).button();
      }
  };

})(jQuery);