
(function($) {

  Drupal.behaviors.commercePosProductSearch = {
      attach:function(context, settings) {
    	//Button behaviors.
        $('.commerce-pos-search-results-container .entity-commerce-product', context)
          .once('product-add-button').mousedown(function() {
          var sku = $('.commerce-pos-product-search-sku', this).html();
          var posInput = $('.commerce-pos-input');
          var currentText = posInput.val();
          if (currentText == '' || currentText.substring(currentText.length - 1) == '*') {
            posInput.val(currentText + sku);
          }
          else {
            posInput.val(currentText + '*' + sku);
          }
          $('.commerce-pos-ajax-trigger').click();
        }).button();
      }
  };

})(jQuery);