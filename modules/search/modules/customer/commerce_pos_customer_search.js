
(function($) {

  Drupal.behaviors.commercePosCustomerSearch = {
      attach:function(context, settings) {
    	//Button behaviors.
        $('.commerce-pos-search-results-container .entity-commerce-customer-profile', context)
          .once('customer-add-button').mousedown(function() {
          var customerNumber = $('.commerce-pos-customer-search-customer-number', this).html();
          var posInput = $('.commerce-pos-input');
          var currentText = posInput.val();
          posInput.val(customerNumber);
          $('.commerce-pos-new-action-name').val('commerce_pos_customer');
          $('.commerce-pos-ajax-trigger').click();
        }).button();
      }
  };

})(jQuery);