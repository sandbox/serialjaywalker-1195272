
(function($) {

  Drupal.commercePosSearch = {
    buildFacetString : function(wrapper) {
      var searchName = $('.commerce-pos-search-form-search-name', wrapper).val();
      var panel = $('.commerce-pos-search-panel-' + searchName);
      var form = $('.commerce-pos-search-facets-form', panel);
      var string = '';
      if (typeof(Drupal.settings.commercePosSearch.facets) == "object") {
        var facetNames = Drupal.settings.commercePosSearch.facets[searchName];
        if (typeof(facetNames) == "object") {
          $.each(facetNames, function(i, facetName){
            var facetValue = $('input[name=' + facetName + ']:checked', panel).val();
            if (facetValue) {
              string += '&f[' + i +']=' + facetName + ':' + facetValue;
            }
          });
        }
      }
      return string;      
    },
    loadResults : function(wrapper, preserveFacets, page) {
      var searchName = $('.commerce-pos-search-form-search-name', wrapper).val();
      var input = $('.commerce-pos-search-form-query', wrapper);
      var query = input.val();
      var requestQuery = query || "";
      var path = Drupal.settings.basePath + '?q=commerce_pos_search/' + searchName + '/' + encodeURI(query);
      if(preserveFacets) {
        path += '/0';
      }
      if(page) {
        path+='&page=' + page;
      }
      //Get facet string if applicable.
      var facets = Drupal.commercePosSearch.buildFacetString(wrapper);
      if (facets != '') {
        path += facets;
      }

      $.getJSON(path, function(response) {
        //Only replace if the search hasn't been updated again.
        if (query == $('.commerce-pos-search-form-query', wrapper).val()) {
          var container = $('.commerce-pos-search-results', wrapper);
          Drupal.detachBehaviors(container);
          container.html(response.results);
          Drupal.attachBehaviors(container);
          //Now the facets.
          if (!preserveFacets) {
            var facets = $('.commerce-pos-search-facets', wrapper);
            Drupal.detachBehaviors(facets);
            facets.html(response.facets);
            Drupal.attachBehaviors(facets);
            //Store the facet names.
            if (typeof Drupal.settings.commercePosSearch.facets != "object") {
              Drupal.settings.commercePosSearch.facets = {};
            }
            Drupal.settings.commercePosSearch.facets[searchName] = response.facetNames;
          }
        }
      });
    }   
  };
  
  Drupal.behaviors.commercePosSearch = {
      attach:function(context, settings) {
        $('.facetapi-commerce-pos-search-radios').once('buttonset').buttonset();
        //Deal with searches which have been designated for reload on the server end.
        $.each(Drupal.settings.commercePosSearch.refresh, function(name, value) {
          if (value) {
            Drupal.settings.commercePosSearch.refresh[name] = false;
            wrapper = $('.commerce-pos-search-panel-' + name);
            Drupal.commercePosSearch.loadResults(wrapper, false);
          }
        });
        //Key up/down bindings for search query fields
        var queryChangeAction = function(event) {
          //Get new results when the user pauses in typing.
          var input = $(event.target);
          var oldQuery = input.val();
          var searchName = $('.commerce-pos-search-form-search-name', input.parents('form')).val();

          setTimeout(function() {
            var newQuery = input.val();
            if (newQuery == oldQuery && newQuery != queryChangeAction.query) {
              //Clear existing facets values.
              var panel = input.parents('.commerce-pos-search-panel');
              $('.facetapi-commerce-pos-search-radios:checked', panel).attr('checked', false);
              Drupal.commercePosSearch.loadResults(panel, false);
              queryChangeAction.query = newQuery;
            }
          }, 150);
        };
        $('.commerce-pos-search-form-query', context).once('commerce-pos-search-no-submit')
        .keydown(function(event) {
          if (event.which == 13) {
            //Don't submit the form when enter is pressed.
            event.preventDefault();
          }
        }).once('commerce-pos-search-update').keyup(function(event){queryChangeAction(event);});
        //Reload results when facets are updated.
       $('.commerce-pos-search-facets form input').once('facet-refresh').change(
          function(event){
            Drupal.commercePosSearch.loadResults($(this).parents('.commerce-pos-search-panel'), true);
          });
       //Support for pagers.
       $('.commerce-pos-search-results .pager a').once('ajax-pager')
        .click(
          function(event) {
          event.preventDefault();
          var url = $(event.target).attr('href');
          var regEx = /[&?]page\=([0-9]+)/;
          var matches = regEx.exec(url);
          var page = (matches === null) ? false : matches[1];
          var searchName = $('.commerce-pos-search-form-search-name', $(event.target).parents('.commerce-pos-search-panel'))
            .val();
          Drupal.commercePosSearch.loadResults($(this).parents('.commerce-pos-search-panel'), true, page);
        });
       //Deal with radio clear buttons.
       $('.commerce-pos-search-radios-clear', context).once('clear')
        .button( "option", "icons", { primary: "ui-icon-closethick"}).button( "option", "text", false)
        .click(function(event) {
          event.preventDefault();
          var facet = $(this).parents('.form-radios');
          //Change the facet value; trigger the change event.
          $('input.facetapi-commerce-pos-search-radios:checked', facet).attr('checked', false).change();
          $('.ui-button.ui-state-active', facet).removeClass('ui-state-active');
       });
       //Deal with query clear buttons.
       $('.commerce-pos-search-query-clear', context).once('clear')
        .button({"icons" : { primary : "ui-icon-closethick"},  "text" : false})
        .click(function(event) {
          event.preventDefault();
          $('.commerce-pos-search-form-query', $(this).parents('form')).val("").keyup();
       });
      }
  //TODO: Add buttons for clearing facet value.
  };

})(jQuery);